For this exercise, we will guide you through a complete example of a CI/CD pipeline for a Python application that will include the following steps :
Build a Python machine learning model
Run basic tests
Export the model
Upload it in S3 if you have a S3 account
